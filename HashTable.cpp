#include "HashTable.h"

template<typename KeyT, typename T> HashTable<KeyT, T>::
HashTable()
{
  for(int i = 0 ; i < TABLESIZE; i++)
    {
      table[i].is_free = true;
    }
}

template<typename KeyT, typename T> unsigned int HashTable<KeyT, T>::
hash(KeyT key)
{
  const unsigned int C = 2654435769u;
  unsigned int shift = (32 - TABLESIZE_POW) / 2;
  return ((C*(unsigned int)key) << shift) & ((1 << TABLESIZE_POW) - 1);
}


template<typename KeyT, typename T> T* HashTable<KeyT, T>::
find(KeyT key)
{
  unsigned int start, index;

  start = index = hash(key);

  //  while(!table[in])
}

template<typename KeyT, typename T> bool HashTable<KeyT, T>::
insert(KeyT key, T t)
{
  unsigned int start, index;
  start = index = hash(key);

  while(!table[index].is_free)
    {
      if(table[index].key == key)
	return false;
      index = (index + 1) % TABLESIZE;
      if(start == index)
	return false;
    }
  table[index].is_free = false;
  table[index].key = key;
  table[index].t = t;
}

