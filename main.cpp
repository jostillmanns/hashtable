#include <iostream>
#include "HashTable.h"

void test()
{
  HashTable<int,std::string> ht;
  std::string *s = ht.find(505);
  std::cout << "s is " << (s ? *s : "null") << std::endl;
  if (!ht.insert(505, "Pedro"))
    std::cout << "insert 505 failed" << std::endl;
  if (!ht.insert(519, "Daniel"))
    std::cout << "insert 505 failed" << std::endl;
  if (!ht.insert(505, "Pedro"))
    std::cout << "insert 505 failed (correct!)" << std::endl;
  s = ht.find(505);
  std::cout << "s is " << (s ? *s : "null") << std::endl;
  s = ht.find(506);
  std::cout << "s is " << (s ? *s : "null") << std::endl;
  // for (HashTable<int,std::string>::iterator it = ht.begin();
  //      it != ht.end(); ++it)
  //   std::cout << it->key << " / " << it->obj << std::endl;
}

int main()
{
  
  return 0;
}
