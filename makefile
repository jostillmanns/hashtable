CC=g++
CFLAGS=-c

all : hashtable

hashtable: HashTable.o main.cpp
	$(CC) -o hashtable HashTable.o main.cpp

HashTable.o: HashTable.h HashTable.cpp
	$(CC) HashTable.h HashTable.cpp $(CFLAGS)
