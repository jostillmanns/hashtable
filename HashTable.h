#ifndef HASHTABLE_H
#define HASHTABLE_H

#define TABLESIZE_POW 5
#define TABLESIZE (1 << TABLESIZE_POW)

template<typename KeyT, typename T>
struct HashTableT
{
  bool is_free;
  KeyT key;
  T t;  
};

template<typename KeyT, typename T>
class HashTable
{
public:
  HashTable();
  unsigned int hash(KeyT);
  bool insert(KeyT, T);
  T* find(KeyT);
private:
  HashTableT<KeyT, T> table[TABLESIZE];
};

# endif
